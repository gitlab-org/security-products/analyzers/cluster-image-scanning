package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/cluster-image-scanning/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/command"
)

func main() {
	app := command.NewApp(metadata.AnalyzerDetails)
	app.Commands = command.NewCommands(command.Config{
		ArtifactName: command.ArtifactNameClusterImageScanning,
		Match:        match,
		Analyze:      analyze,
		AnalyzeFlags: analyzeFlags(),
		Convert:      convert,

		// The scanner details are set here, but we cannot know the scanner
		// version until we retrieve reports. We should have some way
		// of setting the scanner version in the convert stage or
		// tying the scanner version to the vulnerability instead of the report.
		Scanner: metadata.ReportScanner,

		ScanType: metadata.Type,
	})

	// Report optimization uses git to detect redundant vulnerabilities based
	// on file and line number. Since this analyzer does not have associated files,
	// we will disable it internally.
	beforeFunc := func(c *cli.Context) error {
		return c.Set("optimize", "false")
	}
	app.Command("convert").Before = beforeFunc
	app.Command("run").Before = beforeFunc

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
