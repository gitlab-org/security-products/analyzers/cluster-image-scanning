package main

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/aquasecurity/starboard/pkg/apis/aquasecurity/v1alpha1"
	"github.com/aquasecurity/starboard/pkg/starboard"
	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/cluster-image-scanning/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

const trivyScanner = "Trivy"

func convert(input io.Reader, prependPath string) (*report.Report, error) {
	starboardReports := make([]*v1alpha1.VulnerabilityReport, 0)
	if err := json.NewDecoder(input).Decode(&starboardReports); err != nil {
		return nil, fmt.Errorf("failed to decode vulnerability reports: %w", err)
	}

	clusterID := os.Getenv("CIS_CLUSTER_IDENTIFIER")
	agentID := os.Getenv("CIS_CLUSTER_AGENT_IDENTIFIER")

	vulns := []report.Vulnerability{}
	for _, vr := range starboardReports {
		sreport := vr.Report
		scannerName := sreport.Scanner.Name
		if !strings.EqualFold(scannerName, trivyScanner) {
			log.Debugf("skipping report from unsupported scanner: %s", scannerName)
			continue
		}

		for _, v := range sreport.Vulnerabilities {
			vuln := report.Vulnerability{
				Category: metadata.Type,
				Name:     v.VulnerabilityID,

				// Ex: CVE-2020-27350 in apt
				Message: fmt.Sprintf("%s in %s", v.VulnerabilityID, v.Resource),

				// Although (v1alpha1.Vulnerability).Description exists, it seems to always be empty on reports.
				// We will use the title instead.
				Description: v.Title,

				Severity:   convertSeverity(v.Severity),
				Confidence: report.ConfidenceLevelUnknown,
				Solution:   fmt.Sprintf("Upgrade %s from %s to %s", v.Resource, v.InstalledVersion, v.FixedVersion),
				Scanner:    metadata.IssueScanner,
				Location:   convertLocation(vr, v, clusterID, agentID),
				Identifiers: []report.Identifier{
					report.CVEIdentifier(v.VulnerabilityID),
				},
				Links: convertLinks(v),
			}
			vulns = append(vulns, vuln)
		}
	}

	newReport := new(report.Report)
	newReport.Version = report.CurrentVersion()
	newReport.Analyzer = metadata.AnalyzerID
	newReport.Vulnerabilities = vulns

	// We want to set the scanner version here,
	// but report.Scan.Scanner is overridden in command.Run()
	// with the value of command.Config.Scanner
	// We don't have any way to set it after the scan has completed.

	return newReport, nil
}

func convertLocation(sreport *v1alpha1.VulnerabilityReport, vuln v1alpha1.Vulnerability, clusterID string, agentID string) report.Location {
	artifact := sreport.Report.Artifact
	labels := sreport.ObjectMeta.Labels
	return report.Location{
		Dependency: &report.Dependency{
			Package: report.Package{Name: vuln.Resource},
			Version: vuln.InstalledVersion,
		},
		Image: fmt.Sprintf(
			"%s/%s:%s",
			sreport.Report.Registry.Server,
			artifact.Repository,
			artifact.Tag,
		),
		KubernetesResource: &report.KubernetesResource{
			Namespace:     labels[starboard.LabelResourceNamespace],
			Name:          labels[starboard.LabelResourceName],
			Kind:          labels[starboard.LabelResourceKind],
			ContainerName: labels[starboard.LabelContainerName],
			ClusterID:     clusterID,
			AgentID:       agentID,
		},
	}
}

var severityMapping = map[v1alpha1.Severity]report.SeverityLevel{
	v1alpha1.SeverityCritical: report.SeverityLevelCritical,
	v1alpha1.SeverityHigh:     report.SeverityLevelHigh,
	v1alpha1.SeverityMedium:   report.SeverityLevelMedium,
	v1alpha1.SeverityLow:      report.SeverityLevelLow,
	v1alpha1.SeverityNone:     report.SeverityLevelInfo,
	v1alpha1.SeverityUnknown:  report.SeverityLevelUnknown,
}

func convertSeverity(severity v1alpha1.Severity) report.SeverityLevel {
	sev, ok := severityMapping[severity]
	if !ok {
		return report.SeverityLevelUnknown
	}
	return sev
}

func convertLinks(vuln v1alpha1.Vulnerability) []report.Link {
	links := make([]report.Link, 0)
	if vuln.PrimaryLink != "" {
		links = append(links, report.Link{URL: vuln.PrimaryLink})
	}

	for _, l := range vuln.Links {
		links = append(links, report.Link{URL: l})
	}
	return links
}
