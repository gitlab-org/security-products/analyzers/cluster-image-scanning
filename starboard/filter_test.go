package starboard_test

import (
	"testing"

	"k8s.io/apimachinery/pkg/labels"

	"gitlab.com/gitlab-org/security-products/analyzers/cluster-image-scanning/starboard"
)

var (
	kinds          = []string{"Pod", "Node"}
	namespaces     = []string{"alpha", "beta"}
	resourceNames  = []string{"foo", "bar"}
	containerNames = []string{"gamma", "delta"}
)

func filterArguments(kinds []string, namespaces []string, resourceNames []string, containerNames []string) *starboard.FilterArguments {
	return &starboard.FilterArguments{
		Kinds:          kinds,
		Namespaces:     namespaces,
		ResourceNames:  resourceNames,
		ContainerNames: containerNames,
	}
}

func TestFilter(t *testing.T) {
	testcases := []struct {
		name   string
		args   *starboard.FilterArguments
		labels labels.Set

		expectedRequirements int
		expectedMatch        bool
	}{
		{
			name:                 "Empty",
			args:                 filterArguments([]string{}, []string{}, []string{}, []string{}),
			labels:               labels.Set{"starboard.resource.kind": "Deployment"},
			expectedRequirements: 0,
			expectedMatch:        true,
		},
		{
			name:                 "matching kind",
			args:                 filterArguments(kinds, []string{}, []string{}, []string{}),
			labels:               labels.Set{"starboard.resource.kind": kinds[0]},
			expectedRequirements: 1,
			expectedMatch:        true,
		},
		{
			name:                 "mismatching kind",
			args:                 filterArguments(kinds, []string{}, []string{}, []string{}),
			labels:               labels.Set{"starboard.resource.kind": "mismatch"},
			expectedRequirements: 1,
			expectedMatch:        false,
		},
		{
			name:                 "matching namespace",
			args:                 filterArguments([]string{}, namespaces, []string{}, []string{}),
			labels:               labels.Set{"starboard.resource.namespace": namespaces[0]},
			expectedRequirements: 1,
			expectedMatch:        true,
		},
		{
			name:                 "mismatching namespace",
			args:                 filterArguments([]string{}, namespaces, []string{}, []string{}),
			labels:               labels.Set{"starboard.resource.namespace": "mismatch"},
			expectedRequirements: 1,
			expectedMatch:        false,
		},
		{
			name:                 "matching resource name",
			args:                 filterArguments([]string{}, []string{}, resourceNames, []string{}),
			labels:               labels.Set{"starboard.resource.name": resourceNames[1]},
			expectedRequirements: 1,
			expectedMatch:        true,
		},
		{
			name:                 "mismatching resource name",
			args:                 filterArguments([]string{}, []string{}, resourceNames, []string{}),
			labels:               labels.Set{"starboard.resource.name": "mismatch"},
			expectedRequirements: 1,
			expectedMatch:        false,
		},
		{
			name:                 "matching container name",
			args:                 filterArguments([]string{}, []string{}, []string{}, containerNames),
			labels:               labels.Set{"starboard.container.name": containerNames[0]},
			expectedRequirements: 1,
			expectedMatch:        true,
		},
		{
			name:                 "mismatching container name",
			args:                 filterArguments([]string{}, []string{}, []string{}, containerNames),
			labels:               labels.Set{"starboard.container.name": "mismatch"},
			expectedRequirements: 1,
			expectedMatch:        false,
		},
		{
			name:                 "missing label",
			args:                 filterArguments([]string{}, []string{}, []string{}, containerNames),
			labels:               labels.Set{},
			expectedRequirements: 1,
			expectedMatch:        false,
		},
		{
			name: "matching compound",
			args: filterArguments(kinds, namespaces, resourceNames, containerNames),
			labels: labels.Set{
				"starboard.resource.kind":      kinds[0],
				"starboard.resource.namespace": namespaces[0],
				"starboard.resource.name":      resourceNames[0],
				"starboard.container.name":     containerNames[0],
			},
			expectedRequirements: 4,
			expectedMatch:        true,
		},
		{
			name: "mismatching compound",
			args: filterArguments(kinds, namespaces, resourceNames, containerNames),
			labels: labels.Set{
				"starboard.resource.kind":      "mismatch",
				"starboard.resource.namespace": namespaces[0],
				"starboard.resource.name":      resourceNames[0],
				"starboard.container.name":     containerNames[0],
			},
			expectedRequirements: 4,
			expectedMatch:        false,
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			filter, err := starboard.NewFilter(tc.args)
			if err != nil {
				t.Fatal(err)
			}

			sel := filter.Selector()
			reqs, _ := sel.Requirements()

			if len(reqs) != tc.expectedRequirements {
				t.Fatalf("wrong number of requirements: %d", len(reqs))
			}
			if sel.Matches(tc.labels) != tc.expectedMatch {
				t.Fatalf("expected Match() %t, got %t", tc.expectedMatch, sel.Matches(tc.labels))
			}
		})
	}
}
