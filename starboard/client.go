package starboard

import (
	"context"
	"errors"
	"fmt"

	"github.com/aquasecurity/starboard/pkg/apis/aquasecurity/v1alpha1"
	"github.com/aquasecurity/starboard/pkg/kube"
	"github.com/aquasecurity/starboard/pkg/starboard"
	log "github.com/sirupsen/logrus"
	"k8s.io/apimachinery/pkg/api/meta"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/security-products/analyzers/cluster-image-scanning/access"
)

const starboardLink string = "https://aquasecurity.github.io/starboard/latest/operator/installation/helm"
const starboardInstructions string = "Starboard must be installed in the cluster: " + starboardLink

// ErrStarboardNotInstalled signals that the vulnerabilityreports CRD is not present in the cluster.
var ErrStarboardNotInstalled = errors.New(starboardInstructions)

// Client is used to retrieve objects from Starboard
type Client struct {
	// Kube is the Kubernetes client
	Kube client.Client
}

// NewClient creates a new Starboard client using the given kubeconfig
func NewClient(cfg *access.Config) (*Client, error) {
	config, err := cfg.Rest()
	if err != nil {
		return nil, fmt.Errorf("error reading kubeconfig: %w", err)
	}

	scheme := starboard.NewScheme()
	k, err := client.New(config, client.Options{Scheme: scheme})
	if err != nil {
		return nil, fmt.Errorf("error creating kubernetes client: %w", err)
	}
	log.Infof("Using API server %s as Kubernetes control plane", config.Host)

	return &Client{
		Kube: k,
	}, nil
}

// GetVulnerabilityReports returns a list of vulnerability reports which match the
// objects with the given FilterArguments
func (c *Client) GetVulnerabilityReports(ctx context.Context, filterArgs *FilterArguments) ([]v1alpha1.VulnerabilityReport, error) {
	gvrs := make([]string, len(filterArgs.Kinds))
	for i, kind := range filterArgs.Kinds {
		gvr, err := kindToGVR(c, kind)
		if err != nil {
			return nil, err
		}
		gvrs[i] = gvr
	}
	filterArgs.Kinds = gvrs

	filter, err := NewFilter(filterArgs)
	if err != nil {
		return nil, fmt.Errorf("error initializing filters: %w", err)
	}
	matchingLabelsSelector := &client.MatchingLabelsSelector{
		Selector: filter.Selector(),
	}
	if !filter.Empty() {
		log.Infof("Label selectors: %s", matchingLabelsSelector.Selector)
	}
	var list v1alpha1.VulnerabilityReportList
	err = c.Kube.List(ctx, &list, matchingLabelsSelector)
	if _, ok := err.(*meta.NoKindMatchError); ok {
		return nil, ErrStarboardNotInstalled
	}
	if err != nil {
		return nil, err
	}
	return list.Items, nil
}

// kindToGVR translates kinds into their true name. e.g. daemonset -> DaemonSet
func kindToGVR(client *Client, kind string) (string, error) {
	_, gvk, err := kube.GVRForResource(client.Kube.RESTMapper(), kind)
	if err != nil {
		log.Errorf("The CIS_RESOURCE_KIND given (%q) may be invalid", kind)
		return kind, fmt.Errorf("error getting GroupVersionKind for kind %q: %w", kind, err)
	}
	return gvk.Kind, nil
}
