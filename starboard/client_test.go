package starboard_test

import (
	"os"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/cluster-image-scanning/access"
	"gitlab.com/gitlab-org/security-products/analyzers/cluster-image-scanning/starboard"
)

func integrationTestsShouldRun() bool {
	integrationTest := os.Getenv("CIS_INTEGRATION_TEST")
	return strings.EqualFold(integrationTest, "starboard") || strings.EqualFold(integrationTest, "all")
}

func TestNewClient(t *testing.T) {
	if !integrationTestsShouldRun() {
		t.SkipNow()
	}

	kubeconfig, kubecfgProvided := os.LookupEnv("CIS_KUBECONFIG")
	if !kubecfgProvided {
		kubeconfig = os.Getenv("KUBECONFIG")
	}
	projectNamespace := "example"
	projectName := "project"

	cfg := &access.Config{
		KubeconfigPath:   kubeconfig,
		ProjectNamespace: projectNamespace,
		ProjectName:      projectName,
	}

	_, err := starboard.NewClient(cfg)
	if err != nil {
		t.Fatalf("could not create client: %s", err.Error())
	}
}
