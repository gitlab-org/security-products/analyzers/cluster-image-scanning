package access

import (
	"os"
	"testing"
)

const userProvidedKubeconfig = `
apiVersion: v1
kind: Config
clusters:
- name: gitlab-vulnerabilities-viewer
  cluster:
    server: https://1.2.3.4
    certificate-authority-data: fgndshzzvfnnkqoypnxwbyblcjodxcbovwzfdtkuzsiopuhrizdjdjldtpcamtrkpwsggsmvjzxypykdjgtltnskmtqmoxqqeymgsbtgyacpyrmxlpwrhecrpxverfmmhnrwefbtzgkuaiuygercxokapcyrqpnwlewhbxfpfeaibknqamdafxhmnssymiqoqvqnujqfmizrebxsbxcjxdlkodsclhytxahkghdwijmogqauuwrbthyjvstwfptbcehpnrljvfcoggzqeqvilbcodtyehilnujxcxmkuzxsossugjzqadzadenjmcalktjoibpdvbvfdzxvnsjluscripwgnxgbuktzcidhsnmhhqbocqozqbuupdytjretxiroapxyvmvhegzikdtkmqihygvgipkpgowutetkazomntvlynfkabwfstijvyljylryykbgeeeqvoqxrwdfrracjuisecgrjmqamrirbhvpckuhpqvbxanihohlcxcdanpdmxqyzhmernorzhzgkmfrbtvexolhnecmmkvfcgdtyfolqtiadbkhymuhtickfoatylvsanwkwypxzvgxjfoxbkojmswyucdbdxckxiclniqzqnzbjsuyjxqfplwavbeunktkthslbczgqpsuokkttoydddgvvqvnunrmmyaylkwnuwpldcgriterdqswhyichvgtugwbzbqvojxwnfwhagpaikpcjogexkpndagezzagihtvsdslbtnttftdklqmfcravrlcyuirffzqtmdxbmaiczznqeozrzrvadouqdzmhalbauudsyrrjlecngjfnkxdptguyzavovjpcjihdjmpnwbcywnbbgqifknxkickevsayxrjdameaxxiibujwmxeovobzecbnzycjnauyflnvxhznylrxjxvqerjjbyxrckzogbnhjreslkeznjindzmkgweblztizhxomyyajfszxejjssniuxcllnarwvwxuesdlnpksxujjqnutklulnwwsxhkjnkdzhmvylakqynwxalgrpzgccghuipyeqfopljnjlmumtgvyiusiwovsajqdkcjzzvaeiwzaqryjquvphzhsofcmonslbrdtqpkrpwtsljtgpobfdbgwpgyqdgdrhzunxbwjvifcxokgxsajqikzrmmbgcnquvphtvcamrmkxofnzglezcpzrbicaxgvxgywyoomhalcevvskulbdsmpygddqjcooiewsbvbfhspkncswxpzofesusfmrsjcpuouxoaiqqvscmhijienieixqgmukzogmppabpwvoevdioulddutwwwdliogvubrmqoukvmrpgoxgxwrikbvgcqdsewatxaknkojkgwjttflntxadvexbniysfbnfeelrjbpwiynxzetooukyzudjiefktnoagohboryiwocdceyheemmhnhtkuwqexxlafwbehwwpxrgbhmgsfszmvpfkpqndqbpghdulwgytxcncybvomaqgowpyflqrbnchugzpdlhsmvglrwufemwfnwtlsrfqpfmahlgvahhmwsvclbejlttwnurfymftnbafhyzqbsytbymaytqweabtvaelcddorhlxrczwbfsqfzywobbdiueepitakcugvwcajvnpywfyhgsskwpkjkoluivfpzhhqowopyimmxtrkuxiapsrkjwzsfsafgknjgxlrdbveuophrzitfcuaomhhgrfwiezcwndemtgqhhdcbmuboctlfytxispaaokfmtqzfzeorccqzstwvvkjctkioogctjfdzsyrypugplpuxipjtwjpwfpmmxgcqpcezbbqsdmlodqxrcvraawiuqyhhhgaqncqzdapqtxvijkimoegxgddujowoinifjhiyubajjksljxyxejmfuwdvfhemprmqhqevwkvojllcyviwkpeqgsfcagiueaqhnfrikbrfzzmjilclfevegjyreat
contexts:
- name: gitlab-vulnerabilities-viewer
  context:
    cluster: gitlab-vulnerabilities-viewer
    namespace: default
    user: gitlab-vulnerabilities-viewer
current-context: gitlab-vulnerabilities-viewer
users:
- name: gitlab-vulnerabilities-viewer
  user:
    token: vyxatjjmdixqdmagvgabxbroydjzwwlbwsbhkwaswglddmblrcmyvuewwplokooaczkxeeeukkhxbtvditxhswqvdkgnpbnrtoockupradsvitpcwfzqofoucludetdtnwchuvbgjmoxxcgwizvnwewcaxzpkihtzcpzsiycnxhxxtqqcxahzvpuzfmdbloykhbnagtxsobeieojqqbmdhadklbqnithirgrcgvjxbozoxuzichyfcvwfbrbkbcobsxkmlxzurwhwmhiwfhmbdmcdlvoisjpbmgljtatmrjrmhghnhjamtkexdchzfjdwhqvemkongftqzoohnoactmrymyzkaqtcxafibqigpepapkljqwghiyjlxwzfyzozienfpzxytthhilqsxwxdscpkxbjctpwrneljamtsoldwhzdcfsmjwkxcgstvpbvmeqihapfdsqazcamszfrgqzvaimsveyfvrgjtkilmjzpexnnsjvludabvqxycdbdsgavumohcwxavalulzxsylqyewyipguvprcsermbywxpikkrzubbqquytqynfpfaukcukkuowjpdkbyheigghrcnywvkedhphelbiqjsmvqgxqkorvfcjbqaeenlznphnwjdugbrrhvjjliwmmtrtwtzzavqhdnnhmhmwckoheqykeowlsjkhcwpeppymvwukttzxlolflxjgqvsbjdtseralmfwjoysuquasdgitdtetltumslbvduggjubtpvcwhwbgkvtrfjbznzwvxcelrvfxsslawpmpsgbhmujatcxdwakvwnihctcflsxpatzggjuieztqrvaybrdukknrxumocoxoazeokeaypffsualbtirnkgfjaprjssyufpscyeaphksrrcnbydnzjfymwtutpjkhcweampwuxxgyacfxwwmbuvvnumlowccktfedeiwavrhzedtiyiegbcdqrpkdeeqvhiwnrximpicexdvmorwjrlbobyqpiyetxvkfpjzdsludhkkfpmgsolbfybmisgauzvcxqvqzwcsnwzdwnminojnqtmequrxpdpsewzpxsidxvmglnpbomcwzspucmmgeykpazlwzaxswoorfwehhggrrxaebptrirbfulbszjtkyenaujtwbjjaptnqtdjaaujyijxgdsvqbreuvsvyutxgetejchhislgfejzejxjzadmmvidznozgamvtqcdepgiggtepsmnjyslysbbswyorsotdhdzowtqpwjvrjkxsjfvgiijjhrbwbtatweknncmwwmutkuti
`

const ciTunnelKubeconfig = `
apiVersion: v1
kind: Config
clusters:
- name: gitlab
  cluster:
    server: https://kas.gitlab.com/k8s-proxy
users:
- name: agent:1234
  user:
    token: ci:1234:abcdefghijklmnopqrstuvwxyz
contexts:
- name: example/project:test-agent
  context:
    cluster: gitlab
    user: agent:1234
- name: example/group:test-agent
  context:
    cluster: gitlab
    user: agent:1234
`

const projectNamespace = "example"
const projectName = "project"

func TestRawConfig(t *testing.T) {
	testcases := []struct {
		name            string
		kubeContext     string
		kubeconfig      []byte
		expectedContext string
		expectedServer  string
		expectedToken   string
	}{
		{
			name:            "User-provided kubeconfig",
			kubeContext:     "",
			kubeconfig:      []byte(userProvidedKubeconfig),
			expectedContext: "gitlab-vulnerabilities-viewer",
			expectedServer:  "https://1.2.3.4",
			expectedToken:   "vyxatjjmdixqdmagvgabxbroydjzwwlbwsbhkwaswglddmblrcmyvuewwplokooaczkxeeeukkhxbtvditxhswqvdkgnpbnrtoockupradsvitpcwfzqofoucludetdtnwchuvbgjmoxxcgwizvnwewcaxzpkihtzcpzsiycnxhxxtqqcxahzvpuzfmdbloykhbnagtxsobeieojqqbmdhadklbqnithirgrcgvjxbozoxuzichyfcvwfbrbkbcobsxkmlxzurwhwmhiwfhmbdmcdlvoisjpbmgljtatmrjrmhghnhjamtkexdchzfjdwhqvemkongftqzoohnoactmrymyzkaqtcxafibqigpepapkljqwghiyjlxwzfyzozienfpzxytthhilqsxwxdscpkxbjctpwrneljamtsoldwhzdcfsmjwkxcgstvpbvmeqihapfdsqazcamszfrgqzvaimsveyfvrgjtkilmjzpexnnsjvludabvqxycdbdsgavumohcwxavalulzxsylqyewyipguvprcsermbywxpikkrzubbqquytqynfpfaukcukkuowjpdkbyheigghrcnywvkedhphelbiqjsmvqgxqkorvfcjbqaeenlznphnwjdugbrrhvjjliwmmtrtwtzzavqhdnnhmhmwckoheqykeowlsjkhcwpeppymvwukttzxlolflxjgqvsbjdtseralmfwjoysuquasdgitdtetltumslbvduggjubtpvcwhwbgkvtrfjbznzwvxcelrvfxsslawpmpsgbhmujatcxdwakvwnihctcflsxpatzggjuieztqrvaybrdukknrxumocoxoazeokeaypffsualbtirnkgfjaprjssyufpscyeaphksrrcnbydnzjfymwtutpjkhcweampwuxxgyacfxwwmbuvvnumlowccktfedeiwavrhzedtiyiegbcdqrpkdeeqvhiwnrximpicexdvmorwjrlbobyqpiyetxvkfpjzdsludhkkfpmgsolbfybmisgauzvcxqvqzwcsnwzdwnminojnqtmequrxpdpsewzpxsidxvmglnpbomcwzspucmmgeykpazlwzaxswoorfwehhggrrxaebptrirbfulbszjtkyenaujtwbjjaptnqtdjaaujyijxgdsvqbreuvsvyutxgetejchhislgfejzejxjzadmmvidznozgamvtqcdepgiggtepsmnjyslysbbswyorsotdhdzowtqpwjvrjkxsjfvgiijjhrbwbtatweknncmwwmutkuti",
		},
		{
			name:            "CI tunnel kubeconfig",
			kubeContext:     "",
			kubeconfig:      []byte(ciTunnelKubeconfig),
			expectedContext: "example/project:test-agent",
			expectedServer:  "https://kas.gitlab.com/k8s-proxy",
			expectedToken:   "ci:1234:abcdefghijklmnopqrstuvwxyz",
		},
		{
			name:            "KUBE_CONTEXT-provided context",
			kubeContext:     "example/group:test-agent",
			kubeconfig:      []byte(ciTunnelKubeconfig),
			expectedContext: "example/group:test-agent",
			expectedServer:  "https://kas.gitlab.com/k8s-proxy",
			expectedToken:   "ci:1234:abcdefghijklmnopqrstuvwxyz",
		},
	}

	for _, tc := range testcases {
		t.Run(tc.name, func(t *testing.T) {
			file, err := writeKubeconfig(tc.kubeconfig)
			if err != nil {
				t.Fatal(err)
			}
			defer os.Remove(file.Name())

			cfg := Config{
				KubeconfigPath:   file.Name(),
				KubeContext:      tc.kubeContext,
				ProjectNamespace: projectNamespace,
				ProjectName:      projectName,
			}
			rawCfg, err := cfg.rawConfig()
			if err != nil {
				t.Fatal(err)
			}
			if rawCfg.CurrentContext != tc.expectedContext {
				t.Fatalf("expected current context %s, got %s", tc.expectedContext, rawCfg.CurrentContext)
			}
			context, ok := rawCfg.Contexts[rawCfg.CurrentContext]
			if !ok {
				t.Fatal("config lacks current context")
			}
			authInfo, ok := rawCfg.AuthInfos[context.AuthInfo]
			if !ok {
				t.Fatal("config lacks auth info for current context")
			}
			if authInfo.Token != tc.expectedToken {
				t.Fatalf("expected token %s, got %s", tc.expectedToken, authInfo.Token)
			}
			cluster, ok := rawCfg.Clusters[context.Cluster]
			if !ok {
				t.Fatal("config lacks cluster for current context")
			}
			if cluster.Server != tc.expectedServer {
				t.Fatalf("expected expected %s, got %s", tc.expectedServer, cluster.Server)
			}
		})
	}
}

func writeKubeconfig(contents []byte) (*os.File, error) {
	file, err := os.CreateTemp("", "kubeconfig")
	if err != nil {
		return nil, err
	}
	_, err = file.Write(contents)
	if err != nil {
		return nil, err
	}
	err = file.Close()
	if err != nil {
		return nil, err
	}
	return file, nil
}
