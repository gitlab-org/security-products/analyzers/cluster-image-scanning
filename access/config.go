package access

import (
	"errors"
	"fmt"
	"path"
	"strings"

	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	clientcmdapi "k8s.io/client-go/tools/clientcmd/api"
)

// ErrCiTunnelContextNotFound signals that the kubeconfig lacks the context.
var ErrCiTunnelContextNotFound = errors.New("no agent connection contexts found in KUBECONFIG")

// Config determines how a Kubernetes cluster is accessed.
type Config struct {
	KubeconfigPath   string
	KubeContext      string
	ProjectNamespace string
	ProjectName      string
}

// Rest returns a REST config for accessing the cluster, through a
// user-provided kubeconfig, or through the CI tunnel's kubeconfig.
func (c Config) Rest() (*rest.Config, error) {
	rawCfg, err := c.rawConfig()
	if err != nil {
		return nil, err
	}
	return clientcmd.NewDefaultClientConfig(*rawCfg, &clientcmd.ConfigOverrides{}).ClientConfig()
}

func (c Config) rawConfig() (*clientcmdapi.Config, error) {
	path, err := c.kubeconfigPath()
	if err != nil {
		return nil, err
	}
	cfg := c.clientConfig(path)
	rawCfg, err := cfg.RawConfig()
	if err != nil {
		return nil, err
	}
	if c.KubeContext != "" {
		log.Infof("Using KUBE_CONTEXT connection context: %s", c.KubeContext)
		rawCfg.CurrentContext = c.KubeContext
		return &rawCfg, nil
	}
	// the CI tunnel KUBECONFIG does not have a current context set and we need to
	// identify it by name.
	if rawCfg.CurrentContext == "" {
		name, err := c.findCiContext(rawCfg.Contexts)
		if err != nil {
			log.Error("No agent connection contexts found in KUBECONFIG")
			log.Error("Are there any agents set up? https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html")
			return nil, err
		}
		log.Infof("Using agent connection context: %s", name)
		rawCfg.CurrentContext = name
	}
	return &rawCfg, nil
}

func (c Config) clientConfig(kubeconfigPath string) clientcmd.ClientConfig {
	return clientcmd.NewNonInteractiveDeferredLoadingClientConfig(
		&clientcmd.ClientConfigLoadingRules{ExplicitPath: kubeconfigPath},
		&clientcmd.ConfigOverrides{})
}

func (c Config) kubeconfigPath() (string, error) {
	if c.KubeconfigPath != "" {
		return c.KubeconfigPath, nil
	}
	log.Info("KUBECONFIG not set, defaulting to ~/.kube/config")
	home, err := homedir.Dir()
	if err != nil {
		return "", fmt.Errorf("could not find home directory: %w", err)
	}
	return path.Join(home, ".kube", "config"), nil
}

// findCiContext finds the CI tunnel context name.
// The context name has the shape CI_PROJECT_NAMESPACE/CI_PROJECT_NAME:AGENT_NAME,
// however the agent name is not exposed as an environment variable. Hence the
// context is identified by its CI_PROJECT_NAMESPACE/CI_PROJECT_NAME prefix.
func (c Config) findCiContext(contexts map[string]*clientcmdapi.Context) (string, error) {
	prefix := fmt.Sprintf("%s/%s", c.ProjectNamespace, c.ProjectName)
	for name := range contexts {
		if strings.HasPrefix(name, prefix) {
			return name, nil
		}
	}
	return "", ErrCiTunnelContextNotFound
}
