package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/cluster-image-scanning/access"
	"gitlab.com/gitlab-org/security-products/analyzers/cluster-image-scanning/starboard"
)

const (
	kubeconfigFlag       = "kubeconfig"
	containerNamesFlag   = "containers"
	resourceNamesFlag    = "resource-names"
	kindsFlag            = "kinds"
	namespacesFlag       = "namespaces"
	projectNamespaceFlag = "project-namespace"
	projectNameFlag      = "project-name"
	kubeContextFlag      = "kube-context"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    kubeconfigFlag,
			Usage:   "Path to kubeconfig file",
			EnvVars: []string{"CIS_KUBECONFIG", "KUBECONFIG"},
		},
		&cli.StringSliceFlag{
			Name:    containerNamesFlag,
			Usage:   "Name of the container used in the Kubernetes resources to retrieve vulnerabilities for",
			EnvVars: []string{"CIS_CONTAINER_NAMES"},
		},
		&cli.StringSliceFlag{
			Name:    resourceNamesFlag,
			Usage:   "Name of the Kubernetes resources to retrieve vulnerabilities for",
			EnvVars: []string{"CIS_RESOURCE_NAMES"},
		},
		&cli.StringSliceFlag{
			Name:    namespacesFlag,
			Usage:   "Namespace of the Kubernetes resources you would like to retrieve vulnerabilities for",
			EnvVars: []string{"CIS_RESOURCE_NAMESPACES"},
		},
		&cli.StringSliceFlag{
			Name:    kindsFlag,
			Usage:   "Kind of Kubernetes resource to retrieve vulnerabilites for",
			EnvVars: []string{"CIS_RESOURCE_KINDS"},
		},
		&cli.StringFlag{
			Name:    projectNamespaceFlag,
			Usage:   "GitLab project namespace",
			EnvVars: []string{"CI_PROJECT_NAMESPACE"},
		},
		&cli.StringFlag{
			Name:    projectNameFlag,
			Usage:   "GitLab project name",
			EnvVars: []string{"CI_PROJECT_NAME"},
		},
		&cli.StringFlag{
			Name:    kubeContextFlag,
			Usage:   "Kubernetes context",
			EnvVars: []string{"KUBE_CONTEXT"},
		},
	}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	cfg := &access.Config{
		KubeconfigPath:   c.String(kubeconfigFlag),
		KubeContext:      c.String(kubeContextFlag),
		ProjectNamespace: c.String(projectNamespaceFlag),
		ProjectName:      c.String(projectNameFlag),
	}
	client, err := starboard.NewClient(cfg)
	if err != nil {
		return nil, fmt.Errorf("could not initialize Starboard client: %w", err)
	}

	filterArgs := &starboard.FilterArguments{
		Kinds:          c.StringSlice(kindsFlag),
		Namespaces:     c.StringSlice(namespacesFlag),
		ResourceNames:  c.StringSlice(resourceNamesFlag),
		ContainerNames: c.StringSlice(containerNamesFlag),
	}

	reports, err := client.GetVulnerabilityReports(c.Context, filterArgs)
	if err != nil {
		return nil, fmt.Errorf("error retrieving reports from Starboard: %w", err)
	}
	log.Infof("Found %d Starboard vulnerability reports", len(reports))

	// It is not performant to encode and then decode again.
	// Ideally, we should have some method of passing structures
	// to the convert stage instead of a Reader.
	out := new(bytes.Buffer)
	if err := json.NewEncoder(out).Encode(reports); err != nil {
		return nil, fmt.Errorf("failed to encode vulnerability reports: %w", err)
	}

	return io.NopCloser(out), err
}
