# Cluster Image Scanning

## ARCHIVED

This project has been archived. Support for the cluster image scanning analyzer has been [removed](https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/#use-the-cluster-image-scanning-analyzer-removed) in GitLab %15.0.

Use [Cluster image scanning with the GitLab agent ](https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/#cluster-image-scanning-with-the-gitlab-agent) instead.

